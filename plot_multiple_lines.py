#!/usr/bin/env python3

import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import pandas as pd

df = pd.read_csv('data.csv', sep=',')

df.plot()
plt.legend(loc='lower right')
plt.title("Example")
plt.xlabel("xlabel")
plt.ylabel("ylabel")
plt.savefig('example.png', bbox_inches='tight')

